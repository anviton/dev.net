﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Serialisation
{
    /// <summary>
    /// Implémente l'interface <see cref="ISerialiseur"/> pour fournir des fonctionnalités de sérialisation et désérialisation en format binaire.
    /// </summary>
    public class SerialiseurBinaire : ISerialiseur
    {
        /// <summary>
        /// Sérialise un objet en format binaire et le sauvegarde dans un fichier spécifié.
        /// </summary>
        /// <typeparam name="T">Le type de l'objet à sérialiser.</typeparam>
        /// <param name="objet">L'objet à sérialiser.</param>
        /// <param name="cheminFichier">Le nom du fichier où sauvegarder l'objet sérialisé.</param>
        /// <param name="cle">La clé de cryptage (non utilisée dans cette implémentation).</param>
        /// <param name="iv">Le vecteur d'initialisation (non utilisée dans cette implémentation).</param>
        /// <remarks>
        /// Cette implémentation ignore les paramètres de cryptage et effectue une sérialisation binaire directe de l'objet.
        /// Le fichier résultant est stocké dans le dossier de l'application sous AppData.
        /// </remarks>
        public void Serialiser<T>(T objet, string cheminFichier, byte[] cle, byte[] iv)
        {
            string cheminDossierAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string cheminMonApp = Path.Combine(cheminDossierAppData, "TpNote");
            Directory.CreateDirectory(cheminMonApp);
            string cheminFichierComplet = Path.Combine(cheminMonApp, cheminFichier);

            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream flux = new FileStream(cheminFichierComplet, FileMode.Create))
            {
                formatter.Serialize(flux, objet);
            }
        }

        /// <summary>
        /// Désérialise un objet depuis un fichier binaire.
        /// </summary>
        /// <typeparam name="T">Le type de l'objet à désérialiser.</typeparam>
        /// <param name="cheminFichier">Le nom du fichier contenant l'objet sérialisé.</param>
        /// <param name="cle">La clé de cryptage (non utilisée dans cette implémentation).</param>
        /// <param name="iv">Le vecteur d'initialisation (non utilisée dans cette implémentation).</param>
        /// <returns>L'objet de type <typeparamref name="T"/> désérialisé.</returns>
        /// <remarks>
        /// En cas d'erreur lors de la désérialisation, comme un nom de fichier incorrect, la méthode renvoie la valeur par défaut pour le type <typeparamref name="T"/>.
        /// </remarks>
        public T Deserialiser<T>(string cheminFichier, byte[] cle, byte[] iv)
        {
            string cheminDossierAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string cheminMonApp = Path.Combine(cheminDossierAppData, "TpNote");
            Directory.CreateDirectory(cheminMonApp);

            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                string cheminFichierComplet = Path.Combine(cheminMonApp, cheminFichier);
                using (FileStream flux = new FileStream(cheminFichierComplet, FileMode.Open))
                {
                    return (T)formatter.Deserialize(flux);
                }
            }
            catch
            {
                Console.WriteLine("Mauvais nom de fichier");
                return default(T);
            }
        }
    }

}
