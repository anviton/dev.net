﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Serialisation
{
    /// <summary>
    /// Implémente l'interface <see cref="ISerialiseur"/> pour fournir des fonctionnalités de sérialisation et de désérialisation en format XML,
    /// incluant le cryptage et le décryptage des données.
    /// </summary>
    public class SerialiseurXML : ISerialiseur
    {
        /// <summary>
        /// Sérialise un objet en format XML, crypte les données sérialisées et les sauvegarde dans un fichier spécifié.
        /// </summary>
        /// <typeparam name="T">Le type de l'objet à sérialiser.</typeparam>
        /// <param name="objet">L'objet à sérialiser.</param>
        /// <param name="cheminFichier">Le nom du fichier où sauvegarder les données cryptées.</param>
        /// <param name="cle">La clé de cryptage utilisée pour sécuriser les données.</param>
        /// <param name="iv">Le vecteur d'initialisation utilisé pour le cryptage.</param>
        /// <remarks>
        /// Cette méthode réalise une sérialisation XML de l'objet spécifié, puis crypte le résultat XML avant de l'écrire dans le fichier spécifié.
        /// Les données sont stockées dans le dossier de l'application sous AppData.
        /// </remarks>
        public void Serialiser<T>(T objet, string cheminFichier, byte[] cle, byte[] iv)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            string cheminDossierAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string cheminMonApp = Path.Combine(cheminDossierAppData, "TpNote");
            Directory.CreateDirectory(cheminMonApp);
            string cheminFichierComplet = Path.Combine(cheminMonApp, cheminFichier);

            using (var stringWriter = new StringWriter())
            {
                serializer.Serialize(stringWriter, objet);
                string xmlData = stringWriter.ToString();

                byte[] donneesCryptees = CryptageHelper.Crypter(xmlData, cle, iv);

                File.WriteAllBytes(cheminFichierComplet, donneesCryptees);
            }
        }

        /// <summary>
        /// Désérialise un objet depuis des données cryptées stockées dans un fichier spécifié, après avoir décrypté les données.
        /// </summary>
        /// <typeparam name="T">Le type de l'objet à désérialiser.</typeparam>
        /// <param name="cheminFichier">Le nom du fichier contenant les données cryptées.</param>
        /// <param name="cle">La clé de cryptage utilisée pour décrypter les données.</param>
        /// <param name="iv">Le vecteur d'initialisation utilisé pour le décryptage.</param>
        /// <returns>L'objet de type <typeparamref name="T"/> désérialisé.</returns>
        /// <remarks>
        /// Si une erreur survient lors du processus de décryptage ou de désérialisation, comme un mauvais nom de fichier ou un mot de passe incorrect,
        /// un message est affiché et la valeur par défaut pour le type <typeparamref name="T"/> est retournée.
        /// </remarks>
        public T Deserialiser<T>(string cheminFichier, byte[] cle, byte[] iv)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            string cheminDossierAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string cheminMonApp = Path.Combine(cheminDossierAppData, "TpNote");
            Directory.CreateDirectory(cheminMonApp);

            try
            {
                string cheminFichierComplet = Path.Combine(cheminMonApp, cheminFichier);

                byte[] donneesCryptees = File.ReadAllBytes(cheminFichierComplet);

                string xmlDecrypted = CryptageHelper.Decrypter(donneesCryptees, cle, iv);

                using (TextReader reader = new StringReader(xmlDecrypted))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch
            {
                Console.WriteLine("Mauvais nom de fichier, ou Mot de passe incorrect");
                return default(T);
            }
        }
    }

}
