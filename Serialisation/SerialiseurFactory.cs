﻿using System;

namespace Serialisation
{
    /// <summary>
    /// Fournit une méthode factory pour obtenir des instances de sérialiseurs spécifiques, basées sur le type de sérialisation.
    /// </summary>
    public class SerialiseurFactory
    {
        /// <summary>
        /// Enumère les types de sérialisation supportés par la factory.
        /// </summary>
        public enum TypeSerialisation
        {
            /// <summary>
            /// Indique une sérialisation en format binaire.
            /// </summary>
            Binaire,

            /// <summary>
            /// Indique une sérialisation en format XML.
            /// </summary>
            XML
        }

        /// <summary>
        /// Retourne une instance de sérialiseur correspondant au type de sérialisation spécifié.
        /// </summary>
        /// <param name="type">Le type de sérialisation pour lequel un sérialiseur est demandé.</param>
        /// <returns>Une instance de l'interface <see cref="ISerialiseur"/> qui implémente le type de sérialisation spécifié.</returns>
        /// <exception cref="NotImplementedException">Lancée si le type de sérialisation spécifié n'est pas supporté.</exception>
        /// <remarks>
        /// Cette méthode utilise une instruction switch pour déterminer le type de sérialiseur à retourner,
        /// permettant facilement l'ajout de nouveaux types de sérialisation à l'avenir.
        /// </remarks>
        public static ISerialiseur ObtenirSerialiseur(TypeSerialisation type)
        {
            switch (type)
            {
                case TypeSerialisation.Binaire:
                    return new SerialiseurBinaire();
                case TypeSerialisation.XML:
                    return new SerialiseurXML();
                default:
                    throw new NotImplementedException();
            }
        }
    }


}
