﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Serialisation
{
    /// <summary>
    /// Fournit des fonctionnalités de cryptographie, notamment la génération de clés et de vecteurs d'initialisation pour l'algorithme AES.
    /// </summary>
    public class CryptageAES
    {
        /// <summary>
        /// Génère une clé cryptographique et un vecteur d'initialisation (IV) à partir d'un mot de passe et d'un sel.
        /// </summary>
        /// <param name="motDePasse">Le mot de passe utilisé pour générer la clé et l'IV.</param>
        /// <param name="sel">Une chaîne de caractères ajoutée au mot de passe pour renforcer la sécurité et réduire les risques d'attaques par dictionnaire.</param>
        /// <param name="tailleCle">La taille de la clé cryptographique désirée, en bits. La valeur par défaut est 256 bits.</param>
        /// <param name="tailleIV">La taille du vecteur d'initialisation, en bits. La valeur par défaut est 128 bits.</param>
        /// <returns>Un tuple contenant la clé cryptographique et le vecteur d'initialisation générés.</returns>
        /// <remarks>
        /// Utilise l'algorithme PBKDF2 pour dériver la clé et l'IV à partir du mot de passe et du sel. Le nombre d'itérations est fixé à 10 000 pour renforcer la sécurité.
        /// </remarks>
        public static (byte[] cle, byte[] iv) GenererCleEtIV(string motDePasse, string sel, int tailleCle = 256, int tailleIV = 128)
        {
            byte[] selBytes = Encoding.UTF8.GetBytes(sel);

            using (var deriveBytes = new Rfc2898DeriveBytes(motDePasse, selBytes, 10000))
            {
                byte[] cle = deriveBytes.GetBytes(tailleCle / 8);
                byte[] iv = deriveBytes.GetBytes(tailleIV / 8);
                return (cle, iv);
            }
        }
    }


}
