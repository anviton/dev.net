﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Serialisation
{
    /// <summary>
    /// Fournit un contrat pour les classes de sérialisation, incluant des méthodes pour sérialiser et désérialiser des objets,
    /// avec intégration du cryptage et décryptage.
    /// </summary>
    public interface ISerialiseur
    {
        /// <summary>
        /// Sérialise un objet et enregistre les données cryptées dans un fichier spécifié.
        /// </summary>
        /// <typeparam name="T">Le type de l'objet à sérialiser.</typeparam>
        /// <param name="objet">L'objet à sérialiser.</param>
        /// <param name="cheminFichier">Le chemin du fichier où les données sérialisées et cryptées seront enregistrées.</param>
        /// <param name="cle">La clé de cryptage utilisée pour sécuriser les données.</param>
        /// <param name="iv">Le vecteur d'initialisation utilisé pour le cryptage des données.</param>
        /// <remarks>
        /// Cette méthode doit implémenter la logique de sérialisation de l'objet, suivie par le cryptage des données sérialisées
        /// avant leur écriture dans le fichier spécifié.
        /// </remarks>
        void Serialiser<T>(T objet, string cheminFichier, byte[] cle, byte[] iv);

        /// <summary>
        /// Désérialise un objet à partir de données cryptées stockées dans un fichier spécifié.
        /// </summary>
        /// <typeparam name="T">Le type de l'objet à désérialiser.</typeparam>
        /// <param name="cheminFichier">Le chemin du fichier contenant les données cryptées à désérialiser.</param>
        /// <param name="cle">La clé de cryptage utilisée pour décrypter les données.</param>
        /// <param name="iv">Le vecteur d'initialisation utilisé pour le décryptage des données.</param>
        /// <returns>Un objet du type spécifié, reconstruit à partir des données désérialisées.</returns>
        /// <remarks>
        /// Cette méthode doit implémenter la logique de décryptage des données stockées dans le fichier, suivie par leur désérialisation
        /// pour reconstruire l'objet de type <typeparamref name="T"/>.
        /// </remarks>
        T Deserialiser<T>(string cheminFichier, byte[] cle, byte[] iv);
    }


}
