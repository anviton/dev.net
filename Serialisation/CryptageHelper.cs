﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace Serialisation
{

    /// <summary>
    /// Fournit des méthodes statiques pour le cryptage et le décryptage de données à l'aide de l'algorithme AES.
    /// </summary>
    public static class CryptageHelper
    {
        /// <summary>
        /// Crypte un texte en clair à l'aide de l'algorithme AES et des paramètres spécifiés.
        /// </summary>
        /// <param name="texteClair">Le texte à crypter.</param>
        /// <param name="cle">La clé de cryptage AES.</param>
        /// <param name="iv">Le vecteur d'initialisation AES.</param>
        /// <returns>Les données cryptées sous forme d'un tableau d'octets.</returns>
        /// <remarks>
        /// Cette méthode utilise l'algorithme AES pour crypter le texte en clair. Elle nécessite une clé et un vecteur d'initialisation (IV)
        /// spécifiques à AES pour fonctionner correctement. Les données retournées sont prêtes à être stockées ou transmises de manière sécurisée.
        /// </remarks>
        public static byte[] Crypter(string texteClair, byte[] cle, byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                aes.Key = cle;
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
                        {
                            streamWriter.Write(texteClair);
                        }
                        return memoryStream.ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// Décrypte des données cryptées à l'aide de l'algorithme AES et des paramètres spécifiés.
        /// </summary>
        /// <param name="texteCrypte">Les données cryptées sous forme d'un tableau d'octets.</param>
        /// <param name="cle">La clé de cryptage AES utilisée pour crypter les données.</param>
        /// <param name="iv">Le vecteur d'initialisation AES utilisé pour crypter les données.</param>
        /// <returns>Le texte en clair après décryptage.</returns>
        /// <remarks>
        /// Cette méthode inverse le processus effectué par <see cref="Crypter"/>, restituant le texte en clair original à partir des données cryptées.
        /// Elle nécessite la même clé et le même IV que ceux utilisés pour le cryptage.
        /// </remarks>
        public static string Decrypter(byte[] texteCrypte, byte[] cle, byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                aes.Key = cle;
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(texteCrypte))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader(cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }
    }


}
