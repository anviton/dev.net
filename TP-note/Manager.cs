﻿using projet_data;
using Serialisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TP_note
{
    class Manager
    {
        // Dossier racine de la structure de gestion.
        public Dossier Root;

        // Dossier actuellement actif ou sélectionné dans la gestion.
        private Dossier actif;

        // Contrôle de boucle pour la vérification des entrées utilisateur.
        private Boolean verif;

        /// <summary>
        /// Initialise une nouvelle instance de la classe Manager, permettant de charger des données depuis 
        /// un fichier ou de créer un nouveau dossier racine.
        /// </summary>
        /// <param name="lireFichier">Indique si le système doit tenter de charger les données depuis un fichier.</param>
        public Manager(bool lireFichier)
        {
            verif = true;
            while (verif)
            {
                if (lireFichier)
                {
                    Console.WriteLine("Saisir type de fichier 1 = XML, 2 = Binaire : ");
                    string typeStr = Console.ReadLine();
                    Console.WriteLine("Saisir nom fichier : ");
                    string nomFic = Console.ReadLine();
                    if (typeStr == "1")
                    {
                        Console.WriteLine("Saisir mot de passe");
                        string mdp = Console.ReadLine();
                        var (cle, iv) = CryptageAES.GenererCleEtIV(mdp, "monSelUnique");
                        Lire(nomFic, SerialiseurFactory.TypeSerialisation.XML, cle, iv);
                    }
                    else
                    {
                        Lire(nomFic, SerialiseurFactory.TypeSerialisation.Binaire, null, null);
                    }
                }
                else
                {
                    Root = new Dossier("root");
                    verif = false;
                }
                actif = Root;
            }
        }

        /// <summary>
        /// Affiche récursivement les informations d'un dossier, ses contacts et ses sous-dossiers dans la console, avec une indentation pour refléter la structure hiérarchique.
        /// </summary>
        /// <param name="dossier">Le dossier à afficher.</param>
        /// <param name="indentation">La chaîne d'indentation utilisée pour refléter la hiérarchie des dossiers et contacts. Par défaut, il n'y a pas d'indentation.</param>
        /// <remarks>
        /// Cette méthode affiche les détails du dossier spécifié, puis itère sur chacun de ses contacts pour les afficher.
        /// Ensuite, elle appelle récursivement elle-même pour chaque sous-dossier, augmentant l'indentation pour chaque niveau de hiérarchie.
        /// Cela crée une représentation visuelle claire de la structure des dossiers et des contacts.
        /// Le dossier actuellement sélectionné ou 'actif' est mis en évidence par une couleur différente.
        /// </remarks>
        public void Afficher(Dossier dossier, string indentation = "")
        {
            dossier.AfficherDossier(indentation, actif);
            foreach (Contact contact in dossier.Contacts)
            {
                contact.AfficherContact(indentation + "  ");
            }

            foreach (Dossier sousDossier in dossier.SousDossiers)
            {
                Afficher(sousDossier, indentation + "  ");
            }
        }

        /// <summary>
        /// Permet à l'utilisateur d'ajouter un nouveau sous-dossier au dossier actuellement actif.
        /// </summary>
        /// <remarks>
        /// Cette méthode invite l'utilisateur à saisir le nom du nouveau dossier. Elle vérifie ensuite si un dossier portant le même nom existe déjà
        /// à partir du dossier racine. Si le nom est unique, elle crée un nouveau dossier avec ce nom et l'ajoute comme sous-dossier du dossier actif.
        /// Le nouveau dossier devient ensuite le dossier actif. Un message de confirmation est affiché à l'utilisateur.
        /// </remarks>
        public void AjouterDossier()
        {
            string nomDossier = "default";
            verif = true;
            while (verif)
            {
                Console.Write("Entrez le nom du nouveau dossier : ");
                nomDossier = Console.ReadLine();
                verif = false;
                existe(Root, nomDossier);
            }
            Dossier nouveauDossier = new Dossier(nomDossier);
            actif.AjouterSousDossier(nouveauDossier);
            actif = nouveauDossier;
            Console.WriteLine($"Dossier '{nomDossier}' ajouté avec succès.");
        }

        /// <summary>
        /// Invite l'utilisateur à saisir les informations pour créer un nouveau contact, puis l'ajoute au dossier actuellement actif.
        /// </summary>
        /// <remarks>
        /// L'utilisateur est invité à fournir le nom, prénom, email, numéro de téléphone, et adresse pour le nouveau contact.
        /// Une fois ces informations saisies, un nouvel objet <see cref="Contact"/> est créé et ajouté à la liste des contacts
        /// du dossier actif. Un message de confirmation est affiché à l'utilisateur après l'ajout du contact.
        /// </remarks>
        public void AjouterContact()
        {
            Console.Write("Entrez le nom : ");
            string nom = Console.ReadLine();

            Console.Write("Entrez le prénom : ");
            string prenom = Console.ReadLine();

            Console.Write("Entrez l'email : ");
            string email = Console.ReadLine();

            Console.Write("Entrez le numéro de téléphone : ");
            string numeroTelephone = Console.ReadLine();

            Console.Write("Entrez l'adresse : ");
            string adresse = Console.ReadLine();

            Contact nouveauContact = new Contact(nom, prenom, email, numeroTelephone, adresse, actif);
            actif.AjouterContact(nouveauContact);

            Console.WriteLine($"Contact '{nom} {prenom}' ajouté avec succès.");
        }

        /// <summary>
        /// Charge les données d'un dossier à partir d'un fichier, en utilisant le type de sérialisation spécifié et, si fournis, une clé de cryptage et un vecteur d'initialisation.
        /// </summary>
        /// <param name="nomFichier">Le nom du fichier à partir duquel charger les données.</param>
        /// <param name="type">Le type de sérialisation utilisé pour la désérialisation des données (XML ou Binaire).</param>
        /// <param name="cle">La clé de cryptage utilisée pour décrypter les données, si applicable.</param>
        /// <param name="iv">Le vecteur d'initialisation utilisé avec la clé de cryptage, si applicable.</param>
        /// <remarks>
        /// Cette méthode tente de désérialiser le contenu du fichier spécifié en un objet <see cref="Dossier"/>.
        /// Si la désérialisation réussit, l'objet désérialisé devient le nouveau dossier racine (<see cref="Root"/>),
        /// et la vérification (<see cref="verif"/>) est définie sur false, indiquant que le chargement a été réussi.
        /// Si la désérialisation échoue, la vérification reste true, invitant potentiellement l'utilisateur à réessayer.
        /// </remarks>
        private void Lire(string nomFichier, SerialiseurFactory.TypeSerialisation type, byte[] cle, byte[] iv)
        {
            var serialiseur = SerialiseurFactory.ObtenirSerialiseur(type);
            Root = serialiseur.Deserialiser<Dossier>(nomFichier, cle, iv);
            if(Root != null)
            {
                verif = false;
            }
            else
            {
                verif = true;
            }
        }

        /// <summary>
        /// Sauvegarde la structure du dossier racine dans un fichier, en utilisant le type de sérialisation et le cryptage spécifiés.
        /// </summary>
        /// <param name="type">Le type de sérialisation à utiliser pour la sauvegarde (XML ou Binaire).</param>
        /// <param name="cle">La clé de cryptage utilisée pour sécuriser les données, si applicable.</param>
        /// <param name="iv">Le vecteur d'initialisation utilisé avec la clé de cryptage, si applicable.</param>
        /// <remarks>
        /// Cette méthode invite l'utilisateur à saisir le nom du fichier de sauvegarde.
        /// Elle tente ensuite de sérialiser et de sauvegarder la structure du dossier racine dans le fichier spécifié.
        /// En cas de succès, le processus se termine. Si une exception est levée (par exemple, en raison d'un nom de fichier invalide),
        /// un message d'erreur est affiché, et l'utilisateur est invité à réessayer.
        /// </remarks>
        public void Sauvegarder(SerialiseurFactory.TypeSerialisation type, byte[] cle, byte[] iv)
        {
            bool verification = true;
            while (verification)
            {
                try
                {
                    var serialiseur = SerialiseurFactory.ObtenirSerialiseur(type);
                    Console.Write("Entrez le nom du fichier où sauvegarder : ");
                    string chemin = Console.ReadLine();
                    serialiseur.Serialiser(Root, chemin, cle, iv);
                    verification = false;
                }
                catch (Exception)
                {
                    Console.WriteLine("Nom de fichier invalide");
                }
            }
        }

        /// <summary>
        /// Recherche récursivement un dossier par son nom et met à jour le dossier actif si le dossier est trouvé.
        /// </summary>
        /// <param name="rech">Le dossier à partir duquel commencer la recherche.</param>
        /// <param name="nomDossier">Le nom du dossier à rechercher.</param>
        /// <remarks>
        /// Si le nom du dossier est "root", le dossier racine devient le dossier actif.
        /// Sinon, la méthode parcourt récursivement les sous-dossiers à la recherche du dossier spécifié.
        /// La recherche s'arrête dès que le dossier est trouvé, et ce dossier devient le nouveau dossier actif.
        /// Notez que si le dossier est trouvé dans une branche, la recherche continue néanmoins dans les branches suivantes sans mise à jour supplémentaire de 'actif',
        /// ce qui pourrait ne pas être le comportement attendu si plusieurs dossiers portent le même nom.
        /// </remarks>
        public void cherhcherFic(Dossier rech, string nomDossier)
        {
            if(nomDossier == "root")
            {
                actif = Root;
            }
            else
            {
                foreach (Dossier sousDossier in rech.SousDossiers)
                {
                    if (sousDossier.Nom == nomDossier)
                    {
                        actif = sousDossier;
                        break;
                    }
                    cherhcherFic(sousDossier, nomDossier);
                }
            }
        }

        /// <summary>
        /// Vérifie si un dossier avec le nom spécifié existe déjà à partir du dossier donné.
        /// </summary>
        /// <param name="rech">Le dossier à partir duquel commencer la recherche.</param>
        /// <param name="nomDossier">Le nom du dossier à rechercher.</param>
        /// <remarks>
        /// Cette méthode définit la variable 'verif' à 'true' et affiche un message si un dossier avec le nom spécifié est trouvé,
        /// indiquant que le dossier existe déjà. La recherche est effectuée de manière récursive.
        /// </remarks>
        public void existe(Dossier rech, string nomDossier)
        {
            if (nomDossier == "root")
            {
                verif = true;
                Console.WriteLine("Dossier Déjà existant");
            }
            else
            {
                foreach (Dossier sousDossier in rech.SousDossiers)
                {
                    if (sousDossier.Nom == nomDossier)
                    {
                        verif = true;
                        Console.WriteLine("Dossier Déjà existant");
                        break;
                    }
                    cherhcherFic(sousDossier, nomDossier);
                }
            }
        }

    }
}
