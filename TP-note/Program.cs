﻿using Serialisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_note
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager;
            bool quitter = false;
            Console.WriteLine("1 = Charger un fichier de sauvegarde, 2 = Démarrer avec un gestionnaire vide");
            string chargement = Console.ReadLine();
            if(chargement == "1")
            {
                 manager = new Manager(true);
            }
            else
            {
                manager = new Manager(false);
            }
            while (!quitter)
            {
                Console.WriteLine("Gestionnaire de Contacts");
                Console.WriteLine("1. Ajouter un contact");
                Console.WriteLine("2. Voir tous les contacts");
                Console.WriteLine("3. Changer de Dossier Courant");
                Console.WriteLine("4. Ajouter Dossier");
                Console.WriteLine("5. Quitter");
                Console.Write("Entrez votre choix : ");

                string choix = Console.ReadLine();
                Console.Clear();

                switch (choix)
                {
                    case "1":
                        manager.AjouterContact();
                        break;
                    case "2":
                        manager.Afficher(manager.Root);
                        break;
                    case "3":
                        Console.Write("Entrez le nom du dossier : ");
                        string nomDossier = Console.ReadLine();
                        manager.cherhcherFic(manager.Root, nomDossier);
                        break;
                    case "4":
                        manager.AjouterDossier();
                        break;
                    case "5":
                        Console.WriteLine("Saisir type de fichier pour save : 1 = XML, 2 = Binaire : ");
                        string typeStr = Console.ReadLine();
                        if (typeStr == "1")
                        {
                            Console.WriteLine("Saisir mot de passe");
                            string mdp = Console.ReadLine();
                            var (cle, iv) = CryptageAES.GenererCleEtIV(mdp, "monSelUnique");
                            manager.Sauvegarder(Serialisation.SerialiseurFactory.TypeSerialisation.XML, cle, iv);
                        }
                        else
                        {
                            manager.Sauvegarder(Serialisation.SerialiseurFactory.TypeSerialisation.Binaire, null, null);
                        }
                        quitter = true;
                        break;
                    default:
                        Console.WriteLine("Choix non valide. Veuillez réessayer.");
                        break;
                }

            }
        }
    }

}
