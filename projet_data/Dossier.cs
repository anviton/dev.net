﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_data
{
    /// <summary>
    /// Représente un dossier contenant une liste de contacts et une liste de sous-dossiers, permettant une organisation hiérarchique.
    /// </summary>
    [Serializable]
    public class Dossier
    {
        /// <summary>
        /// Obtient ou définit le nom du dossier.
        /// </summary>
        public string Nom { get; set; }

        /// <summary>
        /// Obtient la liste des contacts contenus dans le dossier. Cette liste est en lecture seule de l'extérieur de la classe.
        /// </summary>
        public List<Contact> Contacts { get; private set; }

        /// <summary>
        /// Obtient la liste des sous-dossiers contenus dans le dossier. Cette liste est en lecture seule de l'extérieur de la classe.
        /// </summary>
        public List<Dossier> SousDossiers { get; private set; }

        /// <summary>
        /// Obtient ou définit la date de création du dossier.
        /// </summary>
        public DateTime DateCreation { get; set; }

        /// <summary>
        /// Obtient ou définit la date de la dernière modification du dossier.
        /// </summary>
        public DateTime DateModification { get; set; }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="Dossier"/> avec des collections vides.
        /// </summary>
        /// <remarks>
        /// Ce constructeur crée un dossier sans contacts ni sous-dossiers. 
        /// Il est destiné à être utilisé lors de la création d'un nouveau dossier avant d'y ajouter des contacts ou des sous-dossiers.
        /// Les collections pour les contacts et les sous-dossiers sont initialisées pour éviter les références nulles lors de l'ajout d'éléments.
        /// </remarks>
        public Dossier()
        {
            Contacts = new List<Contact>();
            SousDossiers = new List<Dossier>();
        }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="Dossier"/> avec un nom spécifié, tout en créant des listes vides pour les contacts et les sous-dossiers.
        /// </summary>
        /// <param name="nom">Le nom du dossier.</param>
        /// <remarks>
        /// Ce constructeur définit également la date de création et de modification du dossier à la date et heure actuelles.
        /// L'appel de <see cref="this()"/> assure que les listes de contacts et de sous-dossiers sont initialisées pour éviter les références nulles.
        /// </remarks>

        public Dossier(string nom) : this()
        {
            Nom = nom;
            DateCreation = DateTime.Now;
            DateModification = DateTime.Now;
        }

        /// <summary>
        /// Ajoute un contact à la liste des contacts du dossier.
        /// </summary>
        /// <param name="contact">L'objet <see cref="Contact"/> à ajouter à la liste des contacts.</param>
        /// <remarks>
        /// Cette méthode ajoute directement le contact spécifié à la collection des contacts du dossier.
        /// Elle ne vérifie pas si le contact existe déjà dans la liste, permettant ainsi les doublons.
        /// Assurez-vous que le contact n'est pas null avant de l'appeler pour éviter les exceptions de référence nulle.
        /// </remarks>
        public void AjouterContact(Contact contact)
        {
            Contacts.Add(contact);
        }

        /// <summary>
        /// Ajoute un sous-dossier à la liste des sous-dossiers du dossier courant.
        /// </summary>
        /// <param name="sousDossier">L'objet <see cref="Dossier"/> représentant le sous-dossier à ajouter.</param>
        /// <remarks>
        /// Cette méthode permet d'organiser les dossiers en une structure hiérarchique, en ajoutant le sous-dossier spécifié à la collection des sous-dossiers du dossier courant.
        /// Elle ne procède pas à la vérification des doublons ; ainsi, un même sous-dossier peut être ajouté plusieurs fois si cette méthode est appelée à plusieurs reprises avec le même objet.
        /// Veillez à ce que l'objet `sousDossier` ne soit pas null pour éviter les exceptions lors de l'ajout à la collection.
        /// </remarks>
        public void AjouterSousDossier(Dossier sousDossier)
        {
            SousDossiers.Add(sousDossier);
        }

        /// <summary>
        /// Affiche les informations du dossier, incluant son nom et les dates de création et de dernière modification.
        /// </summary>
        /// <param name="indentation">Une chaîne de caractères utilisée pour indenter l'affichage, permettant une présentation hiérarchique claire.</param>
        /// <param name="courant">Le dossier actuellement sélectionné ou en cours d'examen. Si le dossier courant est égal à ce dossier, les informations seront mises en évidence.</param>
        /// <remarks>
        /// Cette méthode ajuste la couleur du texte dans la console pour le dossier courant, le rendant plus visible dans une structure hiérarchique de dossiers.
        /// Après l'affichage des informations, la couleur du texte est réinitialisée pour les affichages suivants.
        /// La date de création et la date de dernière modification sont formatées selon le standard international (yyyy-MM-dd hh:mm:ss).
        /// </remarks>
        public void AfficherDossier(string identation, Dossier courant)
        {
            if(this == courant)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
            }
            Console.WriteLine(identation + $"Dossier: {this.Nom}, Date de création: {this.DateCreation.ToString("yyyy-MM-dd hh:mm:ss")}, " +
                $"Date de dernière modification : {this.DateModification.ToString("yyyy-MM-dd hh:mm:ss")}");
            Console.ResetColor();
        }
    }

}
