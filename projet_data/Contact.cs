﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_data
{
    /// <summary>
    /// Représente les informations de contact d'une personne.
    /// </summary>
    /// <remarks>
    /// Cette classe est marquée comme sérialisable, permettant son utilisation dans des opérations de sérialisation
    /// pour la persistance ou le transfert des données. Elle contient des informations basiques telles que
    /// le nom, prénom, adresse email, numéro de téléphone et adresse postale.
    /// </remarks>
    [Serializable]
    public class Contact
    {
        /// <summary>Obtient ou définit le nom de famille du contact.</summary>
        public string Nom { get; set; }
        /// <summary>Obtient ou définit le prénom du contact.</summary>

        public string Prenom { get; set; }

        /// <summary>Obtient ou définit l'adresse email du contact.</summary>
        public string Email { get; set; }

        /// <summary>Obtient ou définit le numéro de téléphone du contact.</summary>
        public string NumeroTelephone { get; set; }

        /// <summary>Obtient ou définit l'adresse postale du contact.</summary>
        public string Adresse { get; set; }

        /// <summary>
        /// Obtient ou définit la date et l'heure de création du contact.
        /// </summary>
        /// <remarks>
        /// La date de création est automatiquement définie lors de l'instanciation d'un contact
        /// pour tracer quand le contact a été ajouté au système.
        public DateTime DateCreation { get; set; }

        public Contact() { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="Contact"/> avec les informations détaillées du contact.
        /// </summary>
        /// <param name="nom">Le nom de famille du contact.</param>
        /// <param name="prenom">Le prénom du contact.</param>
        /// <param name="email">L'adresse email du contact.</param>
        /// <param name="numeroTelephone">Le numéro de téléphone du contact.</param>
        /// <param name="adresse">L'adresse postale du contact.</param>
        /// <param name="dossier">Le dossier auquel ce contact est associé.</param>
        /// <remarks>
        public Contact(string nom, string prenom, string email, string numeroTelephone, string adresse, Dossier dossier)
        {
            Nom = nom;
            Prenom = prenom;
            Email = email;
            NumeroTelephone = numeroTelephone;
            Adresse = adresse;
            DateCreation = DateTime.Now;
            dossier.DateModification = DateTime.Now;
        }

        /// <summary>
        /// Affiche les informations du contact dans la console, précédées de l'indentation spécifiée.
        /// </summary>
        /// <param name="indentation">Une chaîne de caractères utilisée pour indenter l'affichage des informations du contact, 
        /// permettant un alignement visuel personnalisé.</param>
        /// <remarks>
        /// Cette méthode affiche une ligne contenant les informations principales du contact : 
        /// nom, prénom, email, numéro de téléphone, adresse et la date de création.
        /// L'indentation permet d'intégrer cet affichage dans des mises en page plus complexes, 
        /// par exemple lors de l'affichage des contacts dans une structure hiérarchique.
        /// La date de création est formatée selon le standard international ISO (yyyy-MM-dd hh:mm:ss).
        /// </remarks>
        public void AfficherContact(string identation)
        {
            string infoContact = identation + $"Nom: {this.Nom}, Prénom: {this.Prenom}, Email: {this.Email}, Téléphone: {this.NumeroTelephone}, Adresse: " +
                $"{this.Adresse}," + $" Date de création : {this.DateCreation.ToString("yyyy-MM-dd hh:mm:ss")}";
            Console.WriteLine(infoContact);
        }

    }

}
